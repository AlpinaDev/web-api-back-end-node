module.exports.procesarRespuesta = procesarRespuesta;

function procesarRespuesta(resp) {
    let objResponse = JSON.parse(resp);
    let moneda = objResponse.result.source;
    let precio = trunc(objResponse.result.amount.toString(), 2);
    let obj = { "moneda": moneda, "precio": precio };
    return obj;
}

module.exports.procesarError = procesarError;

function procesarError(err) {
    let objResponse = err;
    let error = objResponse;
    let objErr = { "error": error.code };
    return objErr;

}


function trunc (x, posiciones = 0) {
  var s = x.toString()
  var l = s.length
  var decimalLength = s.indexOf('.') + 1
  var numStr = s.substr(0, decimalLength + posiciones)
  return Number(numStr)
}