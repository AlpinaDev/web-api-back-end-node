const moduleHelper = require('../helper/helper');

//Strategy pattern
var cotizacion = function() {
    this.moneda;
};

cotizacion.prototype = {
    setStrategy: function(moneda) {
        this.moneda = moneda;
    },

    calculate: function(request, res, objetos) {
		 let getBody = function getBody (path) {
            return new Promise((resolve, reject) => {
                request.get(path, (err, resp, body) => {
                    if (err) {
                        return reject(moduleHelper.procesarError(err));
                    } else {
                        return resolve(moduleHelper.procesarRespuesta(resp.body));
                    }
                })
            })
        };
        return this.moneda.calculate(request, res, objetos, getBody);
    },
	
};

module.exports.cotizacion = cotizacion;

module.exports.Dolar = function() {
    this.calculate = async function(request, res, finalData, getBody) {
        //Aquí se puede especificar la forma de calcular el valor del Dolar.
		//en este caso solamente vamos a consultar el valor a un endpoint específico de la moneda Dolar, pero podría realizarse cualquier tipo de cálculo y procesamiento dependiendo del requerimiento
        await getBody('https://api.valuta.money/v1/quotes/USD/ARS/json?quantity=1&key=1776|ROQWAVhhdbvZYz9kVBz6QzLeo8ooNjM~').then(
            valorResolve => finalData.push(valorResolve)).catch(err => finalData.push(err));
    }
};

module.exports.Euro = function() {
    this.calculate = async function(request, res, finalData, getBody) {
		//Aquí se puede especificar la forma de calcular el valor del Euro.
		//en este caso solamente vamos a consultar el valor a un endpoint específico de la moneda Euro, pero podría realizarse cualquier tipo de cálculo y procesamiento dependiendo del requerimiento
        await getBody('https://api.valuta.money/v1/quotes/EUR/ARS/json?quantity=1&key=1776|ROQWAVhhdbvZYz9kVBz6QzLeo8ooNjM~').then(
            valorResolve => finalData.push(valorResolve)).catch(err => finalData.push(err));
    }
};

module.exports.Real = function() {
    this.calculate = async function(request, res, finalData, getBody) {
		//Aquí se puede especificar la forma de calcular el valor del Real.
		//en este caso solamente vamos a consultar el valor a un endpoint específico de la moneda Real, pero podría realizarse cualquier tipo de cálculo y procesamiento dependiendo del requerimiento
        await getBody('https://api.valuta.money/v1/quotes/BRL/ARS/json?quantity=1&key=1776|ROQWAVhhdbvZYz9kVBz6QzLeo8ooNjM~').then(
            valorResolve => finalData.push(valorResolve)).catch(err => finalData.push(err));
    }
};